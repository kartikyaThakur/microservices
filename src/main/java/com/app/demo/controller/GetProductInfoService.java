package com.app.demo.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;










import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
//import org.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;

@RestController
@RequestMapping("/prod")
public class GetProductInfoService {

	//private static final Logger log = LoggerFactory.getLogger(AtgProductCatalogService.class);
	/*@RequestMapping(value =  "/product", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String product(@RequestParam(value="productId", defaultValue="9082") String productId) {
		RestTemplate restTemplate = new RestTemplate();
		System.out.println("hi");
		String finalOut="";
		try {

			URL url = new URL("http://localhost:7013/rest/model/atg/commerce/catalog/ProductCatalogActor/getProduct?productId=9082");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

			String output;
			String result="";
		System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				result = result + output;
			}
			finalOut = br.toString();
			
			
		    JsonParser parser = new JsonParser();
		    JsonElement element = parser.parse(result);
			JsonObject obj = element.getAsJsonObject();
			finalOut = obj.toString();
			System.out.println(finalOut);
			conn.disconnect();

			String rrData = getRRData();
			if(rrData!=null && rrData!=""){
				finalOut = finalOut+rrData;
			}
		  } catch (MalformedURLException e) {

			e.printStackTrace();

		  } catch (IOException e) {

			e.printStackTrace();

		  }
		return finalOut;

		
    }*/
	
	private static final String FILENAME = "D:\\SOA\\prod_data.txt";
	
	@RequestMapping(value =  "/product", method = RequestMethod.GET, produces="application/json")
	public @ResponseBody String product(@RequestParam(value="productId", defaultValue="9082") String productId) {
		RestTemplate restTemplate = new RestTemplate();
		JsonParser parser = new JsonParser();
		System.out.println("hi");
		String finalOut="";
		
		BufferedReader br = null;
		FileReader fr = null;
		
		try {
			
			/*Object obj = parser.parse(new FileReader(FILENAME));
			JsonObject jsonObject = (JsonObject) obj;
            System.out.println(jsonObject);
            
            JsonArray msg = (JsonArray)jsonObject.get("RR");*/
            
			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);
			
			String sCurrentLine;
			String result="";
			
			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
				result = result + sCurrentLine;
			}
			finalOut = result;
			
			//JsonParser parser = new JsonParser();
		    JsonElement element = parser.parse(finalOut);
			JsonObject obj = element.getAsJsonObject();
			//finalOut = obj.toString();
			//System.out.println(finalOut);
			
			String rrData = getRRData();
			JsonObject combined = new JsonObject();
			if(rrData!=null && rrData!=""){
				JsonElement element1 = parser.parse(rrData);
				JsonObject obj1 = element1.getAsJsonObject();
				if(obj1!=null){
					
					combined.add("Product", obj);
					combined.add("RR", obj1);
					finalOut = combined.toString();
					System.out.println(finalOut);
				}
			}
			
			
			
		   /* JsonParser parser = new JsonParser();
		    JsonElement element = parser.parse(result);
			JsonObject obj = element.getAsJsonObject();
			finalOut = obj.toString();
			System.out.println(finalOut);*/
		
		  }catch (IOException e) {

			e.printStackTrace();

		  } finally{
		  		try {
					if (br != null)
						br.close();
					if (fr != null)
						fr.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		return finalOut;
	}
	
	/*
	 * 
	 * RestTemplate restTemplate = new RestTemplate();
     
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
     
    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
     
    System.out.println(result);
	 */
	public String getRRData(){
		RestTemplate restTemplate = new RestTemplate();
		final String uri = "http://localhost:8080/RRService/rrData";
		String finalOut = "";
		try {
			HttpHeaders headers = new HttpHeaders();
		    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		     
		    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
			
		    if(result != null && result.hasBody() && !result.getBody().isEmpty()){
		    	String res = result.getBody();
		    	finalOut = res;
		    }
		    
			/*URL url = new URL("http://100.107.15.85:8080/RRService/rrData");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				System.out.println("RRService IS DOWN");
			}
			else {
					BufferedReader br = new BufferedReader(new InputStreamReader(
						(conn.getInputStream())));
		
					String output;
					String result="";
				System.out.println("Output from Server .... \n");
					while ((output = br.readLine()) != null) {
						System.out.println(output);
						result = result + output;
					}
					finalOut = br.toString();
					
					conn.disconnect();
			}*/
		  } catch(Exception e){
			  System.out.println("Exception");
		  }
		/*catch (MalformedURLException e) {

			System.out.println("MalformedURLException");

		  } catch (IOException e) {

			System.out.println("IOEXCEPTION");

		  }*//* catch (JSONException e){
			  e.printStackTrace();
		  }*/
		return finalOut;
	}
}
